#BlackBayou.Vimeo

This is an API wrapper for the Vimeo Advanced API.

The goal of this libary is to create a very easy to manage API wrapper for the Vimeo Advanced API.
This is not an exhaustive port of the entire Vimeo Advanced API, however it should be very easy to 
extend to add any required functions. 

## License
This library is released as is without warranty
under the very permissive MIT license. See http://opensource.org/licenses/MIT for more information.

##Dependencies
This library utilizes the following libraries:

*	[DotNetOpenAuth](http://www.dotnetopenauth.net/)	
	Used for OAuth signing of requests

*	[JSON.NET](http://json.codeplex.com/)
	Used to deserialize JSON responses from the Vimeo Api


##Vimeo Information
For more information about the Advanced API, please visit [https://developer.vimeo.com/apis/advanced](https://developer.vimeo.com/apis/advanced).

## Installation
In order to easily install this package use the Nuget console and enter
`>Install-Package BlackBayou.Vimeo`

[Read More about the nuget package](https://www.nuget.org/packages/BlackBayou.Vimeo/)

##Usage

In order to use this library, you must register a new application with Vimeo. To create your 
application, visit [the Vimeo dev center](https://developer.vimeo.com/apps) and create a new application. Upon creation, 
you will receive your Consumer Key, Consumer Secret, Authorization Token, and Authorization Secret. 
All of these keys will be required in order to use the API. 

###Creating an instance of the API wrapper (assuming an object that contains the required key values from Vimeo):

`// Build the token manager
var tokenManager = new InMemoryTokenManager(
	vimeoSettings.ConsumerKey,
	vimeoSettings.ConsumerSecret,
	new Dictionary<string, string> { { vimeoSettings.AccessToken, vimeoSettings.AccessTokenSecret } });

// Build the api client
var apiClient = new VimeoPlusApi(tokenManager, vimeoSettings.AccessToken); `


###Making authenticated calls to Vimeo:

`var getAlbumsResponse = apiClient.Albums.GetAlbums(vimeoSettings.Username);
            
Assert.IsNotNull(getAlbumsResponse);
Assert.IsTrue(getAlbumsResponse.Success);
Assert.IsTrue(getAlbumsResponse.Albums.Items.Count > 0);

var getAlbumVideosResponse = apiClient.Albums.GetAlbumVideos(getAlbumsResponse.Albums.Items[0].Id);
Assert.IsNotNull(getAlbumVideosResponse);
Assert.IsTrue(getAlbumVideosResponse.Success);`

