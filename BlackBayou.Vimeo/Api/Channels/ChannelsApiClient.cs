﻿using System;
using BlackBayou.Vimeo.Api.Channels.Responses;
using DotNetOpenAuth.OAuth.ChannelElements;

namespace BlackBayou.Vimeo.Api.Channels
{
    public class ChannelsApiClient : VimeoPlusApiClientBase
    {
        public enum GetChannelsSortEnum
        {
            Newest,
            Oldest,
            Alphabetical,
            Most_Videos,
            Most_Subscribed,
            Most_Recently_Updated
        }

        public enum GetChannelVideosResponseTypeEnum
        {
            Default,
            Summary,
            Full
        }
 
        #region Constructor
        public ChannelsApiClient(IConsumerTokenManager tokenManager, string accessToken)
            : base(tokenManager, accessToken) { }
        #endregion

        #region Api Method Builder Methods
        private static IApiMethod BuildGetModeratedChannelsRequest(
            GetChannelsSortEnum sort = GetChannelsSortEnum.Alphabetical, 
            int page = 1,
            int pageSize = 50)
        {
            // Validate parameters
            if (pageSize > 50) throw new ArgumentOutOfRangeException("pageSize", "pageSize must be < 50"); 

            // Build the command
            var command = new ApiMethod("vimeo.channels.getModerated");
            command.Arguments.Add("sort", sort.ToString().ToLower());
            command.Arguments.Add("page", page);
            command.Arguments.Add("per_page", pageSize); 

            return command;
        }

        private static IApiMethod BuildGetChannelVideosRequest(
            int channelId,
            int page = 1,
            int pageSize = 50,
            GetChannelVideosResponseTypeEnum responseType = GetChannelVideosResponseTypeEnum.Full
            )
        {
            // Validate parameters
            if (pageSize > 50) throw new ArgumentOutOfRangeException("pageSize", "pageSize must be < 50");

            // Build the command
            var command = new ApiMethod("vimeo.channels.getVideos");
            command.Arguments.Add("channel_id", channelId);
            command.Arguments.Add("page", page);
            command.Arguments.Add("per_page", pageSize);

            switch (responseType)
            {
                case GetChannelVideosResponseTypeEnum.Summary:
                    command.Arguments.Add("summary_response", true);
                    break;
                case GetChannelVideosResponseTypeEnum.Full:
                    command.Arguments.Add("full_response", true);
                    break; 
                default:
                    break; 
            }

            return command;
        }
        #endregion Api Method Builder Methods

        #region Public Methods
        public GetChannelsResponse GetModeratedChannels(string userId,
            GetChannelsSortEnum sort = GetChannelsSortEnum.Alphabetical,
            int page = 1,
            int pageSize = 50)
        {
            // Build the api method to execute
            var method = BuildGetModeratedChannelsRequest(sort, page, pageSize); 

            // Execute the command 
            return ExecuteRequest<GetChannelsResponse>(method);
        }
        public GetChannelVideosResponse GetChannelVideos(
            int channelId,
            int page = 1,
            int pageSize = 50,
            GetChannelVideosResponseTypeEnum responseType = GetChannelVideosResponseTypeEnum.Full)
        {
            // Build the api method to execute
            var method = BuildGetChannelVideosRequest(channelId, page, pageSize, responseType);

            // Execute the command 
            return ExecuteRequest<GetChannelVideosResponse>(method);
        }
        #endregion Public Methods

    }
}
