﻿using Newtonsoft.Json;

namespace BlackBayou.Vimeo.Api.Channels.Responses
{
    public class GetChannelsResponse : ApiResponse
    {
        [JsonProperty("channels")]
        public Model.ChannelCollection Channels { get; set; }
    }
}
