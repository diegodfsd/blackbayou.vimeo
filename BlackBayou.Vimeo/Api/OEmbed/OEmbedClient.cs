﻿using System.Xml.Linq;

namespace BlackBayou.Vimeo.Api.OEmbed
{
    public class OEmbedClient
    {
        public string GetVideoEmbedCode(int videoId,
            int width = 640,
            int maxWidth = -1,
            int height = 400,
            int maxHeight = -1,
            bool byline = true,
            bool title = true,
            bool portrait = true,
            string color = null,
            bool autoPlay = false,
            bool xhtml = true,
            string wmode = "opaque",
            bool iframe = true)
        {
            var url = "http://vimeo.com/api/oembed.xml?url=http%3A//vimeo.com/" + videoId;

            if (width > 0) url += "&width=" + width;
            if (maxWidth > 0) url += "&maxwidth=" + maxWidth;
            if (height > 0) url += "&height=" + height;
            if (maxHeight > 0) url += "&maxheight=" + maxHeight;
            if (!byline) url += "&byline=0";
            if (!title) url += "&title=0";
            if (!portrait) url += "&portrait=0";
            if (!string.IsNullOrEmpty(color)) url += "&color=" + color;
            if (autoPlay) url += "&autoPlay=1";
            if (!xhtml) url += "&xhtml=0";
            if (!string.IsNullOrEmpty(wmode)) url += "&wmode=" + wmode;
            if (!iframe) url += "&iframe=0";

            var feed = XDocument.Load(url);
            return feed.Root.Element("html").Value;
        }
    }
}
