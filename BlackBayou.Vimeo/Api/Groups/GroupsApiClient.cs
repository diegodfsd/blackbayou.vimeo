﻿using System;
using BlackBayou.Vimeo.Api.Groups.Responses;
using DotNetOpenAuth.OAuth.ChannelElements;

namespace BlackBayou.Vimeo.Api.Groups
{
    public class GroupsApiClient : VimeoPlusApiClientBase
    {
        public enum GetGroupsSortEnum
        {
            Newest,
            Oldest,
            Alphabetical,
            Most_Videos,
            Most_Subscribed
        }

        public enum GetGroupVideosResponseTypeEnum
        {
            Default,
            Summary,
            Full
        }

        public enum GetGroupVideosSortEnum
        {
            Newest, 
            Oldest,
            Featured,
            Most_Played,
            Most_Commented,
            Most_Liked,
            Random
        }

        #region Constructor
        public GroupsApiClient(IConsumerTokenManager tokenManager, string accessToken)
            : base(tokenManager, accessToken) { }
        #endregion

        #region Api Method Builder Methods
        private static IApiMethod BuildGetGroupsRequest(
            string userId,
            GetGroupsSortEnum sort = GetGroupsSortEnum.Alphabetical,
            int page = 1,
            int pageSize = 50)
        {
            // Validate parameters
            if (pageSize > 50) throw new ArgumentOutOfRangeException("pageSize", "pageSize must be < 50");

            // Build the command
            var command = new ApiMethod("vimeo.groups.getAll");
            if (!string.IsNullOrEmpty(userId))
            {
                command.Arguments.Add("user_id", userId); 
            }
            command.Arguments.Add("sort", sort.ToString().ToLower());
            command.Arguments.Add("page", page);
            command.Arguments.Add("per_page", pageSize);

            return command;
        }

        private static IApiMethod BuildGetGroupVideosRequest(
            int groupId,
            int page = 1,
            int pageSize = 50,
            GetGroupVideosSortEnum sort = GetGroupVideosSortEnum.Newest,
            GetGroupVideosResponseTypeEnum responseType = GetGroupVideosResponseTypeEnum.Full
            )
        {
            // Validate parameters
            if (pageSize > 50) throw new ArgumentOutOfRangeException("pageSize", "pageSize must be < 50");

            // Build the command
            var command = new ApiMethod("vimeo.groups.getVideos");
            command.Arguments.Add("group_id", groupId);
            command.Arguments.Add("page", page);
            command.Arguments.Add("per_page", pageSize);
            command.Arguments.Add("sort", sort.ToString().ToLower());

            switch (responseType)
            {
                case GetGroupVideosResponseTypeEnum.Summary:
                    command.Arguments.Add("summary_response", true);
                    break;
                case GetGroupVideosResponseTypeEnum.Full:
                    command.Arguments.Add("full_response", true);
                    break;
                default:
                    break;
            }

            return command;
        }
        #endregion Api Method Builder Methods

        #region Public Methods
        public GetGroupsResponse GetGroups(string userId,
            GetGroupsSortEnum sort = GetGroupsSortEnum.Alphabetical,
            int page = 1,
            int pageSize = 50)
        {
            // Build the api method to execute
            var method = BuildGetGroupsRequest(userId, sort, page, pageSize);

            // Execute the command 
            return ExecuteRequest<GetGroupsResponse>(method);
        }
        public GetGroupVideosResponse GetGroupVideos(
            int groupId,
            int page = 1,
            int pageSize = 50,
            GetGroupVideosSortEnum sort = GetGroupVideosSortEnum.Newest,
            GetGroupVideosResponseTypeEnum responseType = GetGroupVideosResponseTypeEnum.Full)
        {
            // Build the api method to execute
            var method = BuildGetGroupVideosRequest(groupId, page, pageSize, sort, responseType);

            // Execute the command 
            return ExecuteRequest<GetGroupVideosResponse>(method);
        }
        #endregion Public Methods

    }
}
