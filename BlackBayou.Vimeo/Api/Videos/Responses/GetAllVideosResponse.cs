﻿using BlackBayou.Vimeo.Model;
using Newtonsoft.Json;

namespace BlackBayou.Vimeo.Api.Videos.Responses
{
    public class GetAllVideosResponse : ApiResponse
    {
        [JsonProperty("videos")]
        public VideoCollection Videos { get; set; }
    }
}
