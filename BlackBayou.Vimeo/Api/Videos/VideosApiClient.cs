﻿using System;
using BlackBayou.Vimeo.Api.Videos.Responses;
using DotNetOpenAuth.OAuth.ChannelElements;

namespace BlackBayou.Vimeo.Api.Videos
{
    public class VideosApiClient : VimeoPlusApiClientBase
    {
        #region Enums
        public enum GetVideosSortOrderEnum
        {
            Default,
            Newest,
            Oldest,
            Most_Played,
            Most_Commented,
            Most_Liked
        }

        public enum GetVideosResponseTypeEnum
        {
            Default,
            Summary,
            Full
        }

        #endregion

        #region Constructor
        public VideosApiClient(IConsumerTokenManager tokenManager, string accessToken)
            : base(tokenManager, accessToken) { }
        #endregion

        #region Api Method builder methods
        private static IApiMethod BuildGetAllVideosRequest(
            string userId, 
            GetVideosSortOrderEnum sort = GetVideosSortOrderEnum.Default,
            int page = 1,
            int pageSize = 50,
            GetVideosResponseTypeEnum responseType = GetVideosResponseTypeEnum.Full)
        {
            if (string.IsNullOrEmpty(userId)) throw new ArgumentNullException("userId");
            if (pageSize > 50) throw new ArgumentOutOfRangeException("pageSize", "pageSize must be less than 50"); 


            var command = new ApiMethod("vimeo.videos.getUploaded");
            command.Arguments.Add("user_id", userId);
            command.Arguments.Add("sort", sort.ToString().ToLower());
            command.Arguments.Add("page", page);
            command.Arguments.Add("per_page", pageSize);
            switch (responseType)
            {
                case GetVideosResponseTypeEnum.Summary:
                    command.Arguments.Add("summary_response", true);
                    break;
                case GetVideosResponseTypeEnum.Full:
                    command.Arguments.Add("full_response", true);
                    break;
                default:
                    break;
            }

            return command;
        }

        private static IApiMethod BuildGetInfoRequest(int videoId)
        {
            var command = new ApiMethod("vimeo.videos.getInfo");
            command.Arguments.Add("video_id", videoId);
            return command; 
        }
        #endregion

        #region Public Methods
        public GetAllVideosResponse GetAllVideos(
            string userId,
            GetVideosSortOrderEnum sort = GetVideosSortOrderEnum.Default,
            int page = 1,
            int pageSize = 50,
            GetVideosResponseTypeEnum responseType = GetVideosResponseTypeEnum.Full)
        {
            var method = BuildGetAllVideosRequest(userId, sort, page, pageSize, responseType);
            return ExecuteRequest<GetAllVideosResponse>(method);
        }

        public GetVideoInfoResponse GetVideoInfo(int videoId)
        {
            var method = BuildGetInfoRequest(videoId);
            return ExecuteRequest<GetVideoInfoResponse>(method); 
        }
        #endregion

    }
}
