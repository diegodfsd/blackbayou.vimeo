﻿using BlackBayou.Vimeo.Api.Albums;
using BlackBayou.Vimeo.Api.Channels;
using BlackBayou.Vimeo.Api.Groups;
using BlackBayou.Vimeo.Api.OEmbed;
using BlackBayou.Vimeo.Api.People;
using BlackBayou.Vimeo.Api.Videos;
using DotNetOpenAuth.OAuth.ChannelElements;

namespace BlackBayou.Vimeo.Api
{
    public class VimeoPlusApi
    {
        public readonly AlbumsApiClient Albums;
        public readonly ChannelsApiClient Channels;
        public readonly GroupsApiClient Groups;
        public readonly OEmbedClient OEmbed; 
        public readonly PeopleApi People; 
        public readonly VideosApiClient Videos;

        #region Constructors
        public VimeoPlusApi(VimeoApiSettings apiSettings)
            : this(apiSettings.ConsumerKey, apiSettings.ConsumerSecret, apiSettings.AccessToken, apiSettings.AccessTokenSecret)
        {
        }

        public VimeoPlusApi(string consumerKey, string consumerSecret, string accessToken, string accessTokenSecret)
            : this(new OAuth.InMemoryTokenManager(consumerKey, consumerSecret, accessToken, accessTokenSecret), accessToken)
        {

        }

        public VimeoPlusApi(IConsumerTokenManager tokenManager, string accessToken)
        {
            Albums = new AlbumsApiClient(tokenManager, accessToken);
            Channels = new ChannelsApiClient(tokenManager, accessToken);
            Groups = new GroupsApiClient(tokenManager, accessToken);
            OEmbed = new OEmbedClient();
            People = new PeopleApi(tokenManager, accessToken);
            Videos = new VideosApiClient(tokenManager, accessToken);
        } 
        #endregion
    }
}
