﻿using BlackBayou.Vimeo.Api.People.Responses;
using DotNetOpenAuth.OAuth.ChannelElements;

namespace BlackBayou.Vimeo.Api.People
{
    public class PeopleApi : VimeoPlusApiClientBase
    {
 
        #region Constructor
        public PeopleApi(IConsumerTokenManager tokenManager, string accessToken)
            : base(tokenManager, accessToken) { }
        #endregion

        #region Api Method Builder Methods
        private static IApiMethod BuildGetInfoRequest(string userId)
        {
            var command = new ApiMethod("vimeo.people.getInfo");

            if (!string.IsNullOrEmpty(userId))
            {
                command.Arguments.Add("user_id", userId);
            }

            return command;
        }
        #endregion Api Method Builder Methods

        #region Public Methods
        public GetInfoResponse GetCurrentUserInfo()
        {
            return GetInfo(null); 
        }
        public GetInfoResponse GetInfo(string userId)
        {
            // Build the api method to execute
            var method = BuildGetInfoRequest(userId); 

            // Execute the command 
            return ExecuteRequest<GetInfoResponse>(method);
        }
        #endregion Public Methods

    }
}
