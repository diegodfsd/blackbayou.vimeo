﻿using BlackBayou.Vimeo.Model;
using Newtonsoft.Json;

namespace BlackBayou.Vimeo.Api.People.Responses
{
    public class GetInfoResponse : ApiResponse
    {
        [JsonProperty("person")]
        public Person Person { get; set; }
    }
}
