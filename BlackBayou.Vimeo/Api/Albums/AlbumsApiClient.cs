﻿using System;
using BlackBayou.Vimeo.Api.Albums.Responses;
using DotNetOpenAuth.OAuth.ChannelElements;

namespace BlackBayou.Vimeo.Api.Albums
{
    public class AlbumsApiClient : VimeoPlusApiClientBase
    {
        public enum GetAlbumsSortOrderEnum
        {
            Default,
            Newest,
            Oldest,
            Most_Played,
            Most_Commented,
            Most_Liked
        }

        public enum GetAlbumVideoResponseTypeEnum
        {
            Default,
            Summary,
            Full
        }
 
        #region Constructor
        public AlbumsApiClient(IConsumerTokenManager tokenManager, string accessToken)
            : base(tokenManager, accessToken) { }
        #endregion

        #region Api Method Builder Methods
        private static IApiMethod BuildGetAlbumsRequest(
            string userId, 
            GetAlbumsSortOrderEnum sort = GetAlbumsSortOrderEnum.Default, 
            int page = 1,
            int pageSize = 50)
        {
            // Validate parameters
            if (string.IsNullOrEmpty(userId)) throw new ArgumentNullException("userId");
            if (pageSize > 50) throw new ArgumentOutOfRangeException("pageSize", "pageSize must be < 50"); 

            // Build the command
            var command = new ApiMethod("vimeo.albums.getAll");
            command.Arguments.Add("user_id", userId);
            command.Arguments.Add("sort", sort.ToString().ToLower());
            command.Arguments.Add("page", page);
            command.Arguments.Add("per_page", pageSize); 

            return command;
        }

        private static IApiMethod BuildGetAlbumVideosRequest(
            int albumId,
            string password = null,
            int page = 1,
            int pageSize = 50,
            GetAlbumVideoResponseTypeEnum responseType =  GetAlbumVideoResponseTypeEnum.Full
            )
        {
            // Validate parameters
            if (pageSize > 50) throw new ArgumentOutOfRangeException("pageSize", "pageSize must be < 50");

            // Build the command
            var command = new ApiMethod("vimeo.albums.getVideos");
            command.Arguments.Add("album_id", albumId);
            if(!string.IsNullOrEmpty(password))
            {
                command.Arguments.Add("password", password); 
            }
            command.Arguments.Add("page", page);
            command.Arguments.Add("per_page", pageSize);

            switch (responseType)
            {
                case GetAlbumVideoResponseTypeEnum.Summary:
                    command.Arguments.Add("summary_response", true);
                    break; 
                case GetAlbumVideoResponseTypeEnum.Full:
                    command.Arguments.Add("full_response", true);
                    break; 
                default:
                    break; 
            }

            return command;
        }
        #endregion Api Method Builder Methods

        #region Public Methods
        public GetAlbumsResponse GetAlbums(string userId,
            GetAlbumsSortOrderEnum sort = GetAlbumsSortOrderEnum.Default,
            int page = 1,
            int pageSize = 50)
        {
            // Build the api method to execute
            var method = BuildGetAlbumsRequest(userId, sort, page, pageSize); 

            // Execute the command 
            return ExecuteRequest<GetAlbumsResponse>(method);
        }
        public GetAlbumVideosResponse GetAlbumVideos(
             int albumId,
            string password = null,
            int page = 1,
            int pageSize = 50,
            GetAlbumVideoResponseTypeEnum responseType = GetAlbumVideoResponseTypeEnum.Full)
        {
            // Build the api method to execute
            var method = BuildGetAlbumVideosRequest(albumId, password, page, pageSize, responseType);

            // Execute the command 
            return ExecuteRequest<GetAlbumVideosResponse>(method);
        }
        #endregion Public Methods

    }
}
