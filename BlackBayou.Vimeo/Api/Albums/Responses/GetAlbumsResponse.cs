﻿using BlackBayou.Vimeo.Model;
using Newtonsoft.Json;

namespace BlackBayou.Vimeo.Api.Albums.Responses
{
    public class GetAlbumsResponse : ApiResponse
    {
        [JsonProperty("albums")]
        public AlbumCollection Albums { get; set; }
    }
}
