﻿using System;
using Newtonsoft.Json;

namespace BlackBayou.Vimeo.Api
{
    public class ApiResponse
    {
        [JsonProperty("generated_in")]
        public decimal GeneratedIn { get; set; }

        [JsonProperty("stat")]
        public string Status { get; set; }

        [JsonProperty("err")]
        public ApiResponseError Error { get; set; }

        public bool Success
        {
            get { return string.Equals(Status, "ok", StringComparison.InvariantCultureIgnoreCase); }
        }
    }
}
