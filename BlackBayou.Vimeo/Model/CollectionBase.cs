﻿using Newtonsoft.Json;

namespace BlackBayou.Vimeo.Model
{
    public class CollectionBase
    {
        [JsonProperty("on_this_page")]
        public int OnThisPage { get; set; }

        [JsonProperty("page")]
        public int Page { get; set; }

        [JsonProperty("perpage")]
        public int Perpage { get; set; }

        [JsonProperty("total")]
        public int Total { get; set; }
    }
}
