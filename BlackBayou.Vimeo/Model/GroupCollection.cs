﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace BlackBayou.Vimeo.Model
{
    public class GroupCollection : CollectionBase
    {
        [JsonProperty("group")]
        public IList<Group> Items { get; set; }

    }
}
