﻿using Newtonsoft.Json;

namespace BlackBayou.Vimeo.Model
{
    public class ThumbnailVideo
    {
        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("owner")]
        public string Owner { get; set; }

        [JsonProperty("title")]
        public string Title { get; set; }

        [JsonProperty("thumbnails")]
        public ThumbnailCollection Thumbnails { get; set; }

    }
}
