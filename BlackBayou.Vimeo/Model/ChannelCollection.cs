﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace BlackBayou.Vimeo.Model
{
    public class ChannelCollection : CollectionBase
    {
        [JsonProperty("channel")]
        public IList<Channel> Items { get; set; }

    }
}
