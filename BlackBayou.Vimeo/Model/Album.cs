﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace BlackBayou.Vimeo.Model
{
    public class Album
    {
        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("title")]
        public string Title { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }

        [JsonProperty("created_on")]
        public DateTime CreatedOn { get; set; }

        [JsonProperty("total_videos")]
        public int TotalVideos { get; set; }

        [JsonProperty("url")]
        public IList<string> Urls { get; set; }

        [JsonProperty("video_sort_method")]
        public string VideoSortMethod { get; set; }

        [JsonProperty("thumbnail_video")]
        public ThumbnailVideo ThumbnailVideo { get; set; }

    }   
}
