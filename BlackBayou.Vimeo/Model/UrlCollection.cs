﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace BlackBayou.Vimeo.Model
{
    public class UrlCollection
    {
        [JsonProperty("url")]
        public IList<Url> Items { get; set; }
    }
}
