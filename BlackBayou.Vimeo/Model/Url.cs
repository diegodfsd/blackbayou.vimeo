﻿using Newtonsoft.Json;

namespace BlackBayou.Vimeo.Model
{
    public class Url
    {
        [JsonProperty("type")]
        public string Type { get; set; }

        [JsonProperty("_content")]
        public string Content { get; set; }
    }
}
