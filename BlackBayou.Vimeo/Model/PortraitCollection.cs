﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace BlackBayou.Vimeo.Model
{
    public class PortraitCollection
    {
        [JsonProperty("portrait")]
        public IList<Thumbnail> Items { get; set; }
    }
}
