﻿using System;
using System.Collections.Generic;
using BlackBayou.Vimeo.Json;
using Newtonsoft.Json;

namespace BlackBayou.Vimeo.Model
{
    public class Person
    {
        [JsonProperty("created_on")]
        public DateTime CreatedOn { get; set; }

        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("is_contact")]
        [JsonConverter(typeof(BooleanConverter))]
        public bool IsContact { get; set; }

        [JsonProperty("is_plus")]
        [JsonConverter(typeof(BooleanConverter))]
        public bool IsPlus { get; set; }

        [JsonProperty("is_pro")]
        [JsonConverter(typeof(BooleanConverter))]
        public bool IsPro { get; set; }

        [JsonProperty("is_subscribed_to")]
        [JsonConverter(typeof(BooleanConverter))]
        public bool IsSubscribedTo { get; set; }

        [JsonProperty("username")]
        public string Username { get; set; }

        [JsonProperty("display_name")]
        public string DisplayName { get; set; }

        [JsonProperty("realname")]
        public string RealName { get; set; }

        [JsonProperty("location")]
        public string Location { get; set; }

        [JsonProperty("url")]
        public IList<string> Urls { get; set; }

        [JsonProperty("bio")]
        public string Bio { get; set; }

        [JsonProperty("number_of_contacts")]
        public int NumberOfContacts { get; set; }

        [JsonProperty("number_of_uploads")]
        public int NumberOfUploads { get; set; }

        [JsonProperty("number_of_likes")]
        public int NumberOfLikes { get; set; }

        [JsonProperty("number_of_videos")]
        public int NumberOfVideos { get; set; }

        [JsonProperty("number_of_videos_appears_in")]
        public int NumberOfVideosAppearsIn { get; set; }

        [JsonProperty("number_of_albums")]
        public int NumberOfAlbums { get; set; }

        [JsonProperty("number_of_channels")]
        public int NumberOfChannels { get; set; }

        [JsonProperty("number_of_groups")]
        public int NumberOfGroups { get; set; }

        [JsonProperty("profileurl")]
        public string ProfileUrl { get; set; }

        [JsonProperty("videosurl")]
        public string VideosUrl { get; set; }

        [JsonProperty("portraits")]
        public PortraitCollection Portraits { get; set; }
    }
}
