﻿using BlackBayou.Vimeo.Json;
using Newtonsoft.Json;

namespace BlackBayou.Vimeo.Model
{
    public class GroupPermissions
    {
        [JsonProperty("is_featured")]
        [JsonConverter(typeof(BooleanConverter))]
        public bool CanUsersApply { get; set; }

        [JsonProperty("group_type")]
        public string GroupType { get; set; }

        [JsonProperty("who_can_add_vids")]
        public string WhoCanAddVideos { get; set; }

        [JsonProperty("who_can_comment")]
        public string WhoCanComment { get; set; }

        [JsonProperty("who_can_create_events")]
        public string WhoCanCreateEvents { get; set; }

        [JsonProperty("who_can_invite")]
        public string WhoCanInvite { get; set; }

        [JsonProperty("who_can_upload")]
        public string WhoCanUpload { get; set; }

        [JsonProperty("who_can_use_forums")]
        public string WhoCanUseForums { get; set; }
    }
}
