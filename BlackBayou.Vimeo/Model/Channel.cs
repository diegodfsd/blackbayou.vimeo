﻿using System;
using System.Collections.Generic;
using BlackBayou.Vimeo.Json;
using Newtonsoft.Json;

namespace BlackBayou.Vimeo.Model
{
    public class Channel
    {
        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("is_featured")]
        [JsonConverter(typeof(BooleanConverter))]
        public bool IsFeatured { get; set; }

        [JsonProperty("is_sponsored")]
        [JsonConverter(typeof(BooleanConverter))]
        public bool IsSponsored { get; set; }

        [JsonProperty("is_subscribed")]
        [JsonConverter(typeof(BooleanConverter))]
        public bool IsSubscribed { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }

        [JsonProperty("created_on")]
        public DateTime CratedDate { get; set; }

        [JsonProperty("modified_on")]
        public DateTime ModifiedDate { get; set; }

        [JsonProperty("total_videos")]
        public int TotalVideos { get; set; }

        [JsonProperty("total_subscribers")]
        public int TotalSubscribers { get; set; }

        [JsonProperty("logo_url")]
        public string LogoUrl { get; set; }

        [JsonProperty("badge_url")]
        public string BadgeUrl { get; set; }

        [JsonProperty("thumbnail_url")]
        public string ThumbnailUrl { get; set; }

        [JsonProperty("url")]
        public IList<string> Urls { get; set; }


        [JsonProperty("layout")]
        public string Layout { get; set; }

        [JsonProperty("theme")]
        public string Theme { get; set; }

        [JsonProperty("privacy")]
        public string Privacy { get; set; }

        [JsonProperty("creator")]
        public Person Creator { get; set; }
    }
}
