﻿using BlackBayou.Vimeo.Api;
namespace BlackBayou.Vimeo.Tests.Settings
{
    internal class VimeoApiTestSettings
    {
        public VimeoApiSettings VimeoSettings { get; set; }
        public string Username { get; set; }
    }
}
