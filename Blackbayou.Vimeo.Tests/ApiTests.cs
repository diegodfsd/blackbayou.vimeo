﻿using BlackBayou.Vimeo.Api;
using BlackBayou.Vimeo.Tests.Settings;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace BlackBayou.Vimeo.Tests
{
    [TestClass]
    public class ApiTests
    {
        private VimeoApiTestSettings vimeoSettings;
        private VimeoPlusApi apiClient; 

        [TestInitialize]
        public void SetupTest()
        {
            // Load the settings from a file that is not under version control for security
            // The settings loader will create this file in the bin/ folder if it doesn't exist
            vimeoSettings = Settings.SettingsLoader.LoadSettings(); 

            // Build the api client
            apiClient = new VimeoPlusApi(vimeoSettings.VimeoSettings); 
        }

        [TestMethod]
        public void Get_All_Videos()
        {
            var videoResponse = apiClient.Videos.GetAllVideos(vimeoSettings.Username);
            Assert.IsNotNull(videoResponse);
            Assert.IsTrue(videoResponse.Success);
        }

        [TestMethod]
        public void Get_Person()
        {
            var personResponse = apiClient.People.GetInfo(vimeoSettings.Username);
            Assert.IsNotNull(personResponse);
            Assert.IsTrue(personResponse.Success);
        }

        [TestMethod]
        public void Get_Albums()
        {
            var getAlbumsResponse = apiClient.Albums.GetAlbums(vimeoSettings.Username);
            
            Assert.IsNotNull(getAlbumsResponse);
            Assert.IsTrue(getAlbumsResponse.Success);
            Assert.IsTrue(getAlbumsResponse.Albums.Items.Count > 0);

            var getAlbumVideosResponse = apiClient.Albums.GetAlbumVideos(getAlbumsResponse.Albums.Items[0].Id);
            Assert.IsNotNull(getAlbumVideosResponse);
            Assert.IsTrue(getAlbumVideosResponse.Success);
        }

        [TestMethod]
        public void Get_Channels()
        {
            var getChannelsResponse = apiClient.Channels.GetModeratedChannels(vimeoSettings.Username, BlackBayou.Vimeo.Api.Channels.ChannelsApiClient.GetChannelsSortEnum.Alphabetical, 1, 50);
            Assert.IsTrue(getChannelsResponse.Success);
            Assert.IsTrue(getChannelsResponse.Channels.Items.Count > 0);

            var getChannelVideos = apiClient.Channels.GetChannelVideos(getChannelsResponse.Channels.Items[0].Id);
            Assert.IsTrue(getChannelVideos.Success);
        }

        [TestMethod]
        public void Get_Groups()
        {
            var getGroupsResponse = apiClient.Groups.GetGroups(null); 
            Assert.IsTrue(getGroupsResponse.Success);
            Assert.IsNotNull(getGroupsResponse.Groups);
            Assert.IsTrue(getGroupsResponse.Groups.Items.Count > 0);

            var getGroupsVideosResponse = apiClient.Groups.GetGroupVideos(getGroupsResponse.Groups.Items[0].Id);
            Assert.IsTrue(getGroupsVideosResponse.Success);
            Assert.IsNotNull(getGroupsVideosResponse.Videos);
        }
    }
}
